# from django.urls import path

# from auction import views

# urlpatterns = [
#     path('', views.PostList.as_view()),
#     path('<int:pk>/', views.PostDetail()),
# ]

from django.conf.urls import url
from django.urls import include
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken import views

from auction.views.view_sets import PostViewSet, StudentViewSet, UniversityViewSet, UserViewSet, FruitTypeViewSet, FruitBreedViewSet, FarmViewSet, ProductViewSet, AuctionBaseViewSet, AuctionTransactionViewSet, MediaFarmViewSet, MediaProductViewSet, MediaAuctionBaseViewSet, Favorite
from auction.views.auctions import AuctionListView
from auction.views.custom_auth_token import CustomAuthToken

from rest_framework_swagger.views import get_swagger_view
schema_view = get_swagger_view(title='Auction API')


router = DefaultRouter()
router.register(r'posts', PostViewSet, base_name='posts')
router.register(r'students', StudentViewSet, base_name='students')
router.register(r'universities', UniversityViewSet, base_name='universities')

router.register(r'users', UserViewSet, base_name='users')
router.register(r'favorites', UserViewSet, base_name='favorites')
router.register(r'fruit_types', FruitTypeViewSet, base_name='fruit_types')
router.register(r'fruit_breeds', FruitBreedViewSet, base_name='fruit_breeds')
router.register(r'farms', FarmViewSet, base_name='farms')
router.register(r'products', ProductViewSet, base_name='products')
router.register(r'auctions', AuctionBaseViewSet, base_name='auction_bases')
router.register(r'auctions/bid', AuctionTransactionViewSet, base_name='auctions')

router.register(r'media/farm', MediaFarmViewSet, base_name='media_farm')
router.register(r'media/product', MediaProductViewSet, base_name='media_product')
router.register(r'media/auction', MediaAuctionBaseViewSet, base_name='media_auction')

urlpatterns = [
    url(r'^auctions/current', AuctionListView.as_view(), name='auctions'),

    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # url(r'^api-token-auth/', views.obtain_auth_token),
    url(r'^api-token-auth/', CustomAuthToken.as_view()),
    url(r'^docs/', schema_view),
]

urlpatterns += router.urls
