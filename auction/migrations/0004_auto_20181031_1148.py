# Generated by Django 2.1.1 on 2018-10-31 11:48

import auction.models
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('auction', '0003_mediaauctionbase_mediaproduct'),
    ]

    operations = [
        migrations.CreateModel(
            name='Favorite',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('auction_base', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='auction.AuctionBase')),
            ],
            options={
                'verbose_name': 'Favorite',
                'verbose_name_plural': 'Favorites',
                'db_table': 'favorites',
            },
        ),
        migrations.AddField(
            model_name='user',
            name='id_card',
            field=models.ImageField(blank=True, upload_to=auction.models.get_id_card_image_path),
        ),
        migrations.AddField(
            model_name='user',
            name='is_agriculture',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='user',
            name='is_merchant',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='user',
            name='is_active',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='favorite',
            name='user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
