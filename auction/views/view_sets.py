# from django.contrib.auth.models import User
from django.conf import settings
from rest_framework import generics, viewsets

from auction.models import (AuctionBase, AuctionTransaction, Farm, FruitType, FruitBreed,
                            Post, Product, Student, University, User, MediaFarm, MediaProduct, MediaAuctionBase, Favorite)
from auction.serializers import (AuctionBaseSerializer,
                                 AuctionTransactionSerializer, FarmSerializer, FruitTypeSerializer,
                                 FruitBreedSerializer, PostSerializer,
                                 ProductSerializer, StudentSerializer,
                                 UniversitySerializer, UserSerializer, MediaFarmSerializer, MediaProductSerializer, MediaAuctionBaseSerializer, FavoriteSerializer)


class PostList(generics.ListCreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer


class StudentViewSet(viewsets.ModelViewSet):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer


class UniversityViewSet(viewsets.ModelViewSet):
    queryset = University.objects.all()
    serializer_class = UniversitySerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class FruitTypeViewSet(viewsets.ModelViewSet):
    queryset = FruitType.objects.all()
    serializer_class = FruitTypeSerializer


class FruitBreedViewSet(viewsets.ModelViewSet):
    queryset = FruitBreed.objects.all()
    serializer_class = FruitBreedSerializer


class FarmViewSet(viewsets.ModelViewSet):
    queryset = Farm.objects.all()
    serializer_class = FarmSerializer


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class AuctionBaseViewSet(viewsets.ModelViewSet):
    queryset = AuctionBase.objects.all()
    serializer_class = AuctionBaseSerializer


class AuctionTransactionViewSet(viewsets.ModelViewSet):
    queryset = AuctionTransaction.objects.all()
    serializer_class = AuctionTransactionSerializer


class MediaFarmViewSet(viewsets.ModelViewSet):
    queryset = MediaFarm.objects.all()
    serializer_class = MediaFarmSerializer


class MediaProductViewSet(viewsets.ModelViewSet):
    queryset = MediaProduct.objects.all()
    serializer_class = MediaProductSerializer


class MediaAuctionBaseViewSet(viewsets.ModelViewSet):
    queryset = MediaAuctionBase.objects.all()
    serializer_class = MediaAuctionBaseSerializer


class FavoriteViewSet(viewsets.ModelViewSet):
    queryset = Favorite.objects.all()
    serializer_class = FavoriteSerializer
