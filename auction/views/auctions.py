from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from auction.models import AuctionBase


class AuctionListView(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        auction_list = []

        auction_bases = AuctionBase.objects.select_related('product', 'product__fruit_breed', 'product__farm'
                                                           ).filter(status='auction')
        for auction_base in auction_bases:
            auction_detail = {
                'product': {
                    'farm': {
                        'user': {
                            'user_name': auction_base.product.farm.user.username,
                            'first_name': auction_base.product.farm.user.first_name,
                            'last_name': auction_base.product.farm.user.last_name,
                        },
                        'name': auction_base.product.farm.name,
                        'address': auction_base.product.farm.address,
                        'district': auction_base.product.farm.district,
                        'province': auction_base.product.farm.province,
                        'area': auction_base.product.farm.area,
                        'num_tree': auction_base.product.farm.num_tree,
                    },
                    'fruit_type': auction_base.product.fruit_breed.fruit_type.name,
                    'fruit_breed': auction_base.product.fruit_breed.name,
                    'area': auction_base.product.area,
                    'num_tree': auction_base.product.num_tree,
                },
                'auction_base': {
                    'is_collect_service': auction_base.is_collect_service,
                    'expected_harvest_output': auction_base.expected_harvest_output,
                    'expected_harvest_date': auction_base.expected_harvest_date,
                    'open_auction_date': auction_base.open_auction_date,
                    'close_auction_date': auction_base.close_auction_date,
                    'open_auction_price': auction_base.open_auction_price,
                    'extra_condition': auction_base.extra_condition,
                    'status': auction_base.status,
                },
            }

            auction_detail['current_auction'] = None
            auction_transaction = auction_base.auctiontransaction_set.last()
            if auction_transaction:
                auction_detail['current_auction'] = {
                    'auction_price': auction_transaction.auction_price if auction_transaction else None,
                    'auction_price_date': auction_transaction.auction_price_date if auction_transaction else None,
                    'user': {
                        'username': auction_transaction.user.username,
                        'first_name': auction_transaction.user.first_name,
                        'last_name': auction_transaction.user.last_name
                    },
                }

            auction_list.append(auction_detail)

        content = {
            # 'user': request.user.username,  # `django.contrib.auth.User` instance.
            # 'auth': request.auth.key,  # None
            'auction_list': auction_list
        }

        return Response(content)
