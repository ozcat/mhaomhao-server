from django.contrib import admin

from auction.models import (AuctionBase, AuctionTransaction, Farm, MediaFarm, FruitType, FruitBreed, Post, Product, Student, University, User, Favorite)


admin.site.register(Post)
admin.site.register(University)
admin.site.register(Student)

admin.site.register(FruitType)
admin.site.register(FruitBreed)
admin.site.register(Farm)
admin.site.register(MediaFarm)
admin.site.register(Product)
admin.site.register(AuctionBase)
admin.site.register(AuctionTransaction)

admin.site.register(User)
admin.site.register(Favorite)
