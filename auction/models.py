import os
from django.db import models
from django.contrib.auth.models import AbstractUser

from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


def get_id_card_image_path(instance, filename):
    return os.path.join('id_card', filename)


class User(AbstractUser):
    bio = models.TextField(max_length=500, blank=True)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    id_card = models.ImageField(upload_to=get_id_card_image_path, blank=True)
    is_verify = models.BooleanField(default=False)
    is_merchant = models.BooleanField(default=False)
    is_agriculture = models.BooleanField(default=False)
    auction_bid_credit = models.FloatField(null=True, blank=True, default=0.0)
    open_auction_credit = models.FloatField(null=True, blank=True, default=0.0)


class Post(models.Model):
    title = models.CharField(max_length=50)
    content = models.TextField()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

class University(models.Model):
    name = models.CharField(max_length=50)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "University"
        verbose_name_plural = "Universities"

    def __unicode__(self):
        return self.name

class Student(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    university = models.ForeignKey(University, related_name="students", on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Student"
        verbose_name_plural = "Students"

    def __unicode__(self):
        return "%s %s" % (self.first_name, self.last_name)


class FruitType(models.Model):
    name = models.TextField(null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "FruitType"
        verbose_name_plural = "FruitTypes"
        db_table = "auction_fruit_types"

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name


class FruitBreed(models.Model):
    fruit_type = models.ForeignKey(FruitType, null=True, blank=True, on_delete=models.CASCADE)

    name = models.TextField(null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "FruitBreed"
        verbose_name_plural = "FruitBreeds"
        db_table = "auction_fruit_breeds"

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name


class Farm(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.CASCADE)

    name = models.TextField(null=True, blank=True)

    # location
    address = models.TextField(null=True, blank=True)
    district = models.TextField(null=True, blank=True)
    province = models.TextField(null=True, blank=True)

    # size
    area = models.FloatField(null=True, blank=True)
    num_tree = models.IntegerField(null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Farm"
        verbose_name_plural = "Farms"
        db_table = "auction_farms"

    def __unicode__(self):
        return '{} - {}'.format(self.name, self.user.username)

    def __str__(self):
        return '{} - {}'.format(self.name, self.user.username)


def get_farm_image_path(instance, filename):
    return os.path.join('public/farm', filename)


class MediaFarm(models.Model):
    farm = models.ForeignKey(Farm, null=True, blank=True, on_delete=models.CASCADE)

    path = models.ImageField(upload_to=get_farm_image_path, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "MediaFarm"
        verbose_name_plural = "MediaFarms"
        db_table = "media_farms"

    def __unicode__(self):
        return "{} - {}".format(self.farm, self.path)

    def __str__(self):
        return "{} - {}".format(self.farm, self.path)


class Product(models.Model):
    farm = models.ForeignKey(Farm, null=True, blank=True, on_delete=models.CASCADE)
    fruit_breed = models.ForeignKey(FruitBreed, null=True, blank=True, on_delete=models.CASCADE)

    # size
    area = models.FloatField(null=True, blank=True)
    num_tree = models.IntegerField(null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Product"
        verbose_name_plural = "Products"
        db_table = "auction_products"

    def __unicode__(self):
        return "{} - {} - {}".format(self.fruit_breed.fruit_type.name, self.fruit_breed, self.farm)

    def __str__(self):
        return "{} - {} - {}".format(self.fruit_breed.fruit_type.name, self.fruit_breed, self.farm)


def get_product_image_path(instance, filename):
    return os.path.join('public/product', filename)


class MediaProduct(models.Model):
    product = models.ForeignKey(Product, null=True, blank=True, on_delete=models.CASCADE)

    path = models.ImageField(upload_to=get_farm_image_path, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "MediaProduct"
        verbose_name_plural = "MediaProducts"
        db_table = "media_products"

    def __unicode__(self):
        return "{} - {}".format(self.product, self.path)

    def __str__(self):
        return "{} - {}".format(self.product, self.path)


class AuctionBase(models.Model):
    product = models.ForeignKey(Product, null=True, blank=True, on_delete=models.CASCADE)

    is_collect_service = models.BooleanField(default=False)
    expected_harvest_output = models.FloatField(null=True, blank=True, default=0.0)

    expected_harvest_date = models.DateTimeField(null=True, blank=True)
    open_auction_date = models.DateTimeField(null=True, blank=True)
    close_auction_date = models.DateTimeField(null=True, blank=True)
    open_auction_price = models.FloatField(null=True, blank=True, default=0.0)
    extra_condition = models.TextField(null=True, blank=True)

    status = models.TextField(null=True, blank=True, default="prepare")

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "AuctionBase"
        verbose_name_plural = "AuctionBases"
        db_table = "auction_bases"

    def __unicode__(self):
        return "{} - {} - {}".format(self.open_auction_date, self.open_auction_price, self.product)

    def __str__(self):
        return "{} - {} - {}".format(self.open_auction_date, self.open_auction_price, self.product)


def get_auction_base_image_path(instance, filename):
    return os.path.join('public/auction_base', filename)


class MediaAuctionBase(models.Model):
    auction_base = models.ForeignKey(AuctionBase, null=True, blank=True, on_delete=models.CASCADE)

    path = models.ImageField(upload_to=get_farm_image_path, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "MediaAuctionBase"
        verbose_name_plural = "MediaAuctionBases"
        db_table = "media_auction_bases"

    def __unicode__(self):
        return "{} - {}".format(self.auction_base, self.path)

    def __str__(self):
        return "{} - {}".format(self.auction_base, self.path)


class AuctionTransaction(models.Model):
    auction_base = models.ForeignKey(AuctionBase, null=True, blank=True, on_delete=models.CASCADE)

    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.CASCADE)

    auction_price = models.FloatField(null=True, blank=True, default=0.0)
    auction_price_date = models.DateTimeField(auto_now=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "AuctionTransaction"
        verbose_name_plural = "AuctionTransactions"
        db_table = "auction_transactions"

    def __unicode__(self):
        return "{} - {} - {}".format(self.auction_price, self.user, self.auction_base)

    def __str__(self):
        return "{} - {} - {}".format(self.auction_price, self.user, self.auction_base)


class Favorite(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.CASCADE)
    auction_base = models.ForeignKey(AuctionBase, null=True, blank=True, on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Favorite"
        verbose_name_plural = "Favorites"
        db_table = "favorites"

    def __unicode__(self):
        return '{} - {}'.format(self.user, self.auction_base)

    def __str__(self):
        return '{} - {}'.format(self.user, self.auction_base)
