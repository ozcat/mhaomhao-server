from rest_framework import serializers

# from django.contrib.auth.models import User
from django.conf import settings
from auction.models import Post, University, Student, FruitType, FruitBreed, Farm, Product, AuctionBase, AuctionTransaction, User, MediaFarm, MediaProduct, MediaAuctionBase, Favorite


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'title', 'content', 'created_at', 'updated_at',)
        model = Post

class UniversitySerializer(serializers.ModelSerializer):
    # students = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    # students = serializers.RelatedField(many=True)
    # students = serializers.SlugRelatedField(many=True, read_only=True, slug_field='first_name')

    class Meta:
        fields = '__all__'
        # fields = ('id', 'name', 'students', 'created_at', 'updated_at',)
        model = University

class StudentSerializer(serializers.ModelSerializer):
    university = UniversitySerializer()

    class Meta:
        # fields = '__all__'
        fields = ('id', 'first_name', 'last_name', 'university', 'created_at', 'updated_at',)
        model = Student


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = User


class FruitTypeSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = FruitType


class FruitBreedSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = FruitBreed


class FarmSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = Farm


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = Product


class AuctionBaseSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = AuctionBase


class AuctionTransactionSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = AuctionTransaction


class MediaFarmSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = MediaFarm


class MediaProductSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = MediaProduct


class MediaAuctionBaseSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = MediaAuctionBase


class FavoriteSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = Favorite
